module type Task = sig
  type 'a task
  val singleton : 'a -> 'a task
  val add : 'a -> all:'a task -> state:'a task -> 'a task
  val iter_state : ('a -> unit) -> 'a task -> unit
  val iter_all : ('a -> unit) -> 'a task -> unit
end

(** a first presentation with doubly-linked lists outside
    the main data structure.
*)
module External_data = struct
(* we use Batteries doubly-linked list module Dllist :
   http://ocaml-batteries-team.github.com/batteries-included/hdoc/BatDllist.html
*)
  open Batteries

  type 'a task = {
    (* data is a placeholder here, there could be any number of fields *)
    data : 'a;
    all : 'a task Dllist.node_t;
    state : 'a task Dllist.node_t;
  }

  let post_init task =
    Dllist.set task.all task;
    Dllist.set task.state task;
    task

  let singleton data =
     (* I feel safer using Obj.magic on a type that is statically,
       locally known to be a pointer type (record); that is the reason
       for using a dummy task rather than a dummy dllist. Delusional. *)
    let dummy_task = Obj.magic 0 in
    let all = Dllist.create dummy_task in
    let state = Dllist.create dummy_task in
    post_init { data; all; state }

  let add data ~all ~state =
    let all = all.all and state = state.state in
    let dummy_task = Dllist.get all in
    let all = Dllist.append all dummy_task in
    let state = Dllist.append state dummy_task in
    post_init { data; all; state }

  let iter_all f t = Dllist.iter (fun {data} -> f data) t.all
  let iter_state f t = Dllist.iter (fun {data} -> f data) t.state
end

module Test(T : Task) = struct
  open T
  let () =
    let t1 = singleton 1 in
    let t2 = add 2 ~all:t1 ~state:t1 in
    let _t3 = add 3 ~all:t2 ~state:t1 in
    print_newline (print_string "all: "; iter_all print_int t1);
    print_newline (print_string "state: "; iter_state print_int t1);
    (* expected: 123, 132 *)
    ()
end

include Test(External_data)

(** a second presentation with links inline *)
module Inline_data = struct
  type 'a task = {
    data : 'a;
    mutable all_next : 'a task;
    mutable all_prev : 'a task;
    mutable state_next : 'a task;
    mutable state_prev : 'a task;
  }

  let singleton data =
    let rec node = {
      data;
      all_next = node;
      all_prev = node;
      state_next = node;
      state_prev = node;
    } in node

  let add data ~all ~state =
    let rec node = {
      data;
      all_next = all.all_next;
      all_prev = all;
      state_next = state.state_next;
      state_prev = state;
    } in
    all.all_next <- node;
    state.state_next <- node;
    node

  (** For the implementation of 'iter', I exhibit a more factorized
      style where the iteration operation is abstracted over the
      {get,next,prev} accesses of the dllist -- so that it can use
      either the 'all' or the 'state' list.
      
      This genericity comes at a cost in performance (indirect calls),
      and of course it's also possible to write two non-generic versions
      of the traversals.
  *)
  type ('node, 'data) dllist_obj = {
    get : 'node -> 'data;
    next : 'node -> 'node;
    prev : 'node -> 'node;
  }

  let all_obj = {
    get = (fun t -> t.data);
    next = (fun t -> t.all_next);
    prev = (fun t -> t.all_prev);
  }

  let state_obj = {
    get = (fun t -> t.data);
    next = (fun t -> t.state_next);
    prev = (fun t -> t.state_prev);
  }

  let iter obj f init =
    let init_data = obj.get init in
    let rec loop t =
      let data = obj.get t in
      if data != init_data then begin
        f data;
        loop (obj.next t);
      end in
    f init_data;
    loop (obj.next init)

  let iter_all f t = iter all_obj f t
  let iter_state f t = iter state_obj f t
end

include Test(Inline_data)
